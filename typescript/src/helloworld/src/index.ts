//console.log('Hello world!');

const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req: any, res: { statusCode: number; setHeader: (arg0: string, arg1: string) => void; end: (arg0: string) => void; }) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');

  let v1: (number|boolean) = 2;


  const c1 = 2;
  const  c2 = true;
  const  c3 = [5, 7];
  const  c4 = ["two", true, "y"];
  const  c5 = [5, 7];
const  c6 = ["andrew", "ben", "john"];

var [r, s] : number[] = [12, 10];

var [r,]  : number[] = [12, 10];

var  [, s]  : number[]= [12, 10];

var  [r, r]  : number[] = [12, 10];

var  u  : number[] = [1, 2, 3, 4];

var  v  : number[] = [5, 6];

var w  : number[] = [...u, ...v];

var  [h, ...t] = w;

// ##########################""

function explode(s: string): string[] {
  return s.split("");
  }
  function implode(a: string[]): string {
  return a.join("");
  }
  function rev(a: string[]): string[] {
  return a.reverse();
  }
  function hd(a: string[]): string {
  const [h, ...t] = a
  return h;
  }
  function tl(a: string[]): string[] {
  const [h, ...t] = a
  return t;
  }

  
  function first (s: string) : string {
    return hd(explode(s));
  }

  function second (s: string) : string {
    return hd(tl(explode(s)));
  }

  function third (s:string) : string {
    return hd(tl(tl(explode(s))));
  }

  function fourth (s:string) : string {
    return hd(tl(tl(tl(explode(s)))));
  }

  function roll(s: string): string {
    return implode([fourth(s), first(s), second(s), third(s)]);
  }
  
  function exch(s: string): string {
    return implode([second(s), first(s), third(s), fourth(s)]);
  }    
  
  function what(s:string) :string {
    return roll(roll(roll(exch(roll(s)))));
  }

  res.end(what("beau"));


});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});