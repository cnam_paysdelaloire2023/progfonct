import React from 'react';
import { Box, Button, CardHeader, CardBody } from 'grommet';
import { CardFooter } from 'grommet';
import { Favorite, ShareOption } from 'grommet-icons';

import { FC } from 'react';

const Card: FC<{ name: string, email: string}> =
({ name, email}) => {
    return (
        <Box height="small" width="small" background="light-1">
        <CardHeader pad="medium">{name}</CardHeader>
        <CardBody pad="medium">{email}</CardBody>
          <CardFooter pad={{horizontal: "small"}} background="light-2">
            <Button
              icon={< Favorite color="red"/>}
              hoverIndicator
            />
            <Button 
            icon={< ShareOption />} 
            hoverIndicator />
          </CardFooter>
      </Box>
    )
}
export default Card;