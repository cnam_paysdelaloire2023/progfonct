import React from 'react';
import { Grommet, grommet, Box, Button, MouseClick } from 'grommet';
import type { Geo, Address, Company, User } from './Users';
import { users } from './Users';
import Card from './Card';
import { useReducer } from 'react';
import { FunctionComponent } from 'react';


function App() {
    return (
        <Grommet theme={grommet}>
            <Box align="center">
                <Button
                    label="hello world"
                    primary
                    onClick={() => alert('hello, world')}
                />
            </Box>
              {users.map((user: User) =>
                <Card name={user.name} email={user.email} />
              )}
        </Grommet>

    );
}

export default App;