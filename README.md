NFP119 - Programmation Fonctionnelle : des concepts aux applications web
===

# Dépot
https://gitlab.com/cnam_paysdelaloire2023/progfonct

# React JS
https://www.w3schools.com/react/default.asp
https://fr.legacy.reactjs.org/community/examples.html
https://www.commentcoder.com/projets-react-debutants/
https://fr.legacy.reactjs.org/docs/getting-started.html#learn-react
https://fr.legacy.reactjs.org/docs/hello-world.html

# Cours
- https://cedric.cnam.fr/sys/crolard/enseignement/index.html
- https://cedric.cnam.fr/sys/crolard/enseignement/USAL3A/supports.html

# Installation OCaml
https://fdopen.github.io/opam-repository-mingw/installation/

# Plateforme Learn Ocaml
https://ocaml-sf.org/learn-ocaml-public/#

# Documentation
- https://v2.ocaml.org/releases/5.0/htmlman/index.html
- Cours Louis Le Grand : https://info-llg.fr/option-mpsi/?a=cours
- https://www-apr.lip6.fr/~chaillou/Public/DA-OCAML/
- https://dev.realworldocaml.org/

# Python Tutor
https://pythontutor.com

# OCaml en ligne
- https://try.ocamlpro.com/

# Modules
- https://v2.ocaml.org/api/List.html
- https://v2.ocaml.org/api/String.html


# Snippet code OCaml
## Problème page 67 du cours
```
let rec cdanss (c: char) (s: string) : bool =
    let length = String.length s in 
    if length = 0 then false
    else if s.[0] = c then true else
    if length = 1 then false else
    let sous_chaine = String.sub 1 (length-1) in 
    cdanss c sous_chaine
;;
```

```
let dans c s =
    let rec dans_aux c' s' n' =
        if n' < 0 then false else
        s'.[n'] = c' || dans_aux c' s' (n'-1) in
    dans_aux c s ((String.length s) -1 )
;;
```

## Problème page 76
```
let digit c = '0' <= c && c <= '9';; 

let char_to_string c = String.make 1 c;;

let rec extract_digit_from_string (s: string) : string =
  let rec extract_aux (s: string) (n : int) : string =
    if n < 0 then "" else
    let sn = if digit s.[n] then char_to_string s.[n] else "" in
    sn ^ extract_aux s (n-1) in 
  extract_aux s ((String.length s)-1);;
```

## Filtrage page 125
```
let jour_suivant p = 
    match p with
    | (31, 12, a) -> (1, 1, a+1)
    | (31, m, a) -> (1, m+1, a)
    | (j, m, a) -> if j < 28 then (j+1, m, a) else (1, m+1, a);;
val jour_suivant : int * int * int -> int * int * int = <fun>
─( 14:03:39 )─< command 14 >────────────────────────────────────────{ counter: 0 }─
utop # jour_suivant (31, 12, 2023);;
- : int * int * int = (1, 1, 2024)
─( 14:21:39 )─< command 15 >────────────────────────────────────────{ counter: 0 }─
utop # jour_suivant (31, 5, 2023);;
- : int * int * int = (1, 6, 2023)
─( 14:22:20 )─< command 16 >────────────────────────────────────────{ counter: 0 }─
utop # jour_suivant (14, 8, 2023);;
- : int * int * int = (15, 8, 2023)
─( 14:22:30 )─< command 17 >────────────────────────────────────────{ counter: 0 }─
utop # jour_suivant (28, 2, 2023);;
- : int * int * int = (1, 3, 2023)

```

```
 let est_vide (l: 'a list) : bool =
match l with
| [] -> true
| _  -> false;;
val est_vide : 'a list -> bool = <fun>
─( 14:32:40 )─< command 21 >────────────────────────────────────────{ counter: 0 }─
utop # est_vide [1;2;3];;
- : bool = false
─( 14:33:33 )─< command 22 >────────────────────────────────────────{ counter: 0 }─
utop # est_vide [];;
- : bool = true
─( 14:33:47 )─< command 23 >────────────────────────────────────────{ counter: 0 }─
utop # est_vide ["1";"2";"3"];;
- : bool = false

```

```
utop # let rec my_list_length (l : 'a list) : int =
match l with
| [] -> 0
| head::tail -> 1 + my_list_length tail;;
val my_list_length : 'a list -> int = <fun>

─( 14:34:02 )─< command 25 >────────────────────────────────────────{ counter: 0 }─
utop # my_list_length [1;2;3;56];;
- : int = 4

```

## TP 3 
1. Exercice 1
```
 let rec my_list_length2 = function
| [] -> 0
| head::tail -> 1 + my_list_length2 tail;;
val my_list_length2 : 'a list -> int = <fun>
─( 14:36:59 )─< command 27 >────────────────────────────────────────{ counter: 0 }─
utop # my_list_length2 [];;
- : int = 0
─( 15:29:12 )─< command 28 >────────────────────────────────────────{ counter: 0 }─
utop # my_list_length2 ["fg"; "rr"; "hfhfhf"];;
- : int = 3

```

2. Exercice 2
```
let rec rev (l: 'a list) : 'a list = 
match l with
| [] -> []
| [e] -> [e]
| head::tail -> (rev tail) @ [head];;

val rev : 'a list -> 'a list = <fun>
─( 15:33:31 )─< command 31 >────────────────────────────────────────{ counter: 0 }─
utop # rev [];;
- : 'a list = []
─( 15:33:47 )─< command 32 >────────────────────────────────────────{ counter: 0 }─
utop # rev [5];;
- : int list = [5]
─( 15:33:55 )─< command 33 >────────────────────────────────────────{ counter: 0 }─
utop # rev [1;2;3;4;5];;
- : int list = [5; 4; 3; 2; 1]

```

```
let rec concat (l: 'a list list) : 'a list = 
match l with
| [] -> []
| head::tail -> head @ concat tail;;
val concat : 'a list list -> 'a list = <fun>
─( 15:34:08 )─< command 35 >────────────────────────────────────────{ counter: 0 }─
utop # concat [[1;2;3]; [4;5]; [7]; [8;9;10]];;
- : int list = [1; 2; 3; 4; 5; 7; 8; 9; 10]

```

```
 let rev_append (l0: 'a list) (l1: 'a list) : 'a list =
(rev l0) @ l1;; 
val rev_append : 'a list -> 'a list -> 'a list = <fun>
─( 16:38:12 )─< command 38 >────────────────────────────────────────{ counter: 0 }─
utop # rev_append [1;2;3] [4;5;6];;
- : int list = [3; 2; 1; 4; 5; 6]

```

```
utop # let rec rev_append (l0: 'a list) (l1: 'a list) : 'a list =
match l0, l1 with
| [], _ -> l1
| head::tail, _ -> rev_append tail [head]@l1;;
val rev_append : 'a list -> 'a list -> 'a list = <fun>
─( 16:43:03 )─< command 41 >────────────────────────────────────────{ counter: 0 }─
utop # rev_append [1;2;3] [4;5;6];;
- : int list = [3; 2; 1; 4; 5; 6]
```

```
 let rec nth (l: 'a list) (i: int) : 'a =
match i, l with
| 0, head::_ -> head 
| _, head::tail -> nth tail (i-1);; 
Characters 41-112:
Warning 8: this pattern-matching is not exhaustive.
Here is an example of a case that is not matched:
(_, [])
val nth : 'a list -> int -> 'a = <fun>
─( 16:57:54 )─< command 48 >────────────────────────────────────────{ counter: 0 }─
utop # nth [1;2;3] 1;;
- : int = 2

```

```
 let rec last (l : 'a list) : 'a =
match l with
| [e] -> e
| head::tail -> last tail;;
Characters 34-83:
Warning 8: this pattern-matching is not exhaustive.
Here is an example of a case that is not matched:
[]
val last : 'a list -> 'a = <fun>
─( 16:59:18 )─< command 50 >────────────────────────────────────────{ counter: 0 }─
utop # last [1;2;3];;
- : int = 3

```

```
let rec take (l: 'a list) (i:int) : 'a list =
match i, l with 
| 0, _ -> []
| _, head::tail ->  [head] @ take tail (i-1)
| _, _ -> [];;  
val take : 'a list -> int -> 'a list = <fun>
─( 17:06:05 )─< command 55 >────────────────────────────────────────{ counter: 0 }─
utop # take [1;2;3;4;5;6;7;8;9] 3;;
- : int list = [1; 2; 3]
```

```
let rec drop (l: 'a list) (i: int) : 'a list =
match i, l with
| 0, _ -> l
| _, [] -> l
| _, head::tail -> drop tail (i-1);;
val drop : 'a list -> int -> 'a list = <fun>
─( 17:09:06 )─< command 58 >────────────────────────────────────────{ counter: 0 }─
utop # drop [1;2;3;4;5;6;7;8;9] 3;;
- : int list = [4; 5; 6; 7; 8; 9]

```

