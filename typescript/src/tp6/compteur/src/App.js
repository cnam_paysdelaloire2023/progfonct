import React, { useState } from 'react';

const CounterPage = () => {
    const [count, setCount] = useState(0);

    const increment = () => {
        setCount(count + 1);
    };

    const decrement = () => {
        if (count > 0) {
            setCount(count - 1);
        }
    };

    const reset = () => {
        setCount(0);
    };

    return (
        <div>
            <h1>Compteur : {count}</h1>
            <button onClick={increment}>Incrementer</button>
            <button onClick={decrement}>Decrementer</button>
            <button onClick={reset}>Reinitialiser</button>
        </div>
    );
};

export default CounterPage;
