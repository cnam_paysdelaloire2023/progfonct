Prise en Main TypeScript
===

# Tutoriel
https://khalilstemmler.com/blogs/typescript/node-starter-project/

# Configuration d'un répertoire comme projet node/typescript
1. ```mkdir monprojet```
2. ```cd monprojet```
3. ```npm init -y``` (-y pour initiliser le fichier package.json avec les valeurs par défaut)
4. ```npm install typescript --save-dev```
5. ```npm install @types/node --save-dev```
6. 
```
npx tsc --init --rootDir src --outDir build \
--esModuleInterop --resolveJsonModule --lib es6 \
--module commonjs --allowJs true --noImplicitAny true
```
7. ```mkdir src```
8. Créer le fichier ```src\index.ts```
9. Compiler : ```npx tsc```
10. Executer ```node build/index.js```

# Utilisation des modules et import
- Création du fichier ```src/TP1.ts```
- Import de la fonction ```run``` dans le fichier ```index.ts```
```
import {run} from "./TP1";
```

# Configuration test dans navigateur
- Création de la fonction ```function runInServer( s : string)```


# TP5
https://cedric.cnam.fr/sys/crolard/enseignement/USAL3A/grommet-cards.html

- Création dépot
```
npx create-react-app@3.4.1 cards --template typescript
```

- Installation des dépendances
```
npm install grommet grommet-icons styled-components --save-dev
```

- Nettoyage répertoire
```
rm src/App.css src/App.test.tsx src/logo.svg
rm src/index.css src/reportWebVitals.ts
```

- Démarrage App dans navigateur
```npm start```


