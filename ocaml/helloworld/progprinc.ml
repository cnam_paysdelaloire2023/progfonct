
(* Compilation : ocamlc -o progprinc progprinc.ml *)

(* string -> () *)
let affichage chaine = print_string chaine

(* Une fonction: () -> () *)
let helloworld = print_string "Hello World!\n"

(* Point d'entrée du programme () -> ()*)
let () = helloworld; affichage "Hello World2!\n"