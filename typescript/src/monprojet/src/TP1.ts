function exo1() : void
{
    console.log("Exo1");
}

function exo2() : string
{
    return ("Exo2");
}

function runInServer( s : string) {
/*
https://nodejs.org/en/docs/guides/getting-started-guide

1. Executer node index.js
2. Dans le navigateur aller à l'adresse http://127.0.0.1:3000/

*/

const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((_req: any, res: { statusCode: number; setHeader: (arg0: string, arg1: string) => void; end: (arg0: string) => void; }) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end(s);
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

}

export function run()
{
    exo1();
    runInServer(exo2());
}